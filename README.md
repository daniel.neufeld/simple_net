# Simple Net

Simple Net is simple neural network API developed from scratch using numpy. It is intended as a demonstration of how to configure and use Sphinx to document python projects. 
The included documentation contains examples of best practice for API documentation including docstrings, equations, code examples, figures, and other examples.
The Simple Net program was developed following the online textbook called Neural Networks and Deep Learning by Michael Nelson at http://neuralnetworksanddeeplearning.com, though it improves upon the presented code in many respects.

## Dependencies
* python 3.6 +
* numpy
* sphinx
* sphinx_rtd_theme

See the [install.md](install.md) file for instructions on how to make sure the sphinx tutorial package is registered inside your python environment.

## Project Structure

- **docs**: Sphinx documentation files
- **simple_net**: Main simple_net modules
  - **functions.py**: Contains activation functions and derivatives
  - **models.py**: Build and train dense neural nets
  - **datasets.py**: Load and process training and validation data
  - **data**: Folder containing training data
    - **digits_data.pkl**: Gzipped pickle dump containing 8x8 image data for handwritten numbers serving as example data for the simple_net program.

## References

* http://www.sphinx-doc.org/en/master/usage/quickstart.html
* http://neuralnetworksanddeeplearning.com
