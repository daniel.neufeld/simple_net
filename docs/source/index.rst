.. Simple Net documentation master file, created by
   sphinx-quickstart on Mon Sep 30 10:16:19 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Simple Net Documentation
************************

.. image:: /images/logo.png
	:width: 451px
	:alt: logo
	:align: center

Simple Net is a python package intended to demonstrate API documentation using Sphinx. The package itself demonstrates how to develop a simple fully connected neural network from scratch.

.. toctree::
	:maxdepth: 2
	:caption: Contents:

	introduction
	sphinx
	documenting
	simple_net

Indices and tables
******************

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
