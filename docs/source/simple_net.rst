.. _simple_net:

Simple Net API
**************

Simple Net is an API for building fully connected neural networks for machine learning applications. It is primarily intended to provide a simple codebase for those who wish to learn about how neural networks work. The code was developed by following the theory presented in an online textbook called Neural Networks and Deep Learning [#neural_guide]_, though many improvements were made to the code structure to improve both performance and readability.

The Simple Net source code consists of 3 modules. The datasets module contains tools for loading and processing image data. The functions module contains activation functions and derivative functions. The models module contains classes and functions for building neural networks and making predictions.

datasets
========

.. automodule:: datasets
    :members:
    :undoc-members:
    :show-inheritance:

functions
=========

.. automodule:: functions
    :members:
    :undoc-members:
    :show-inheritance:

models
======

.. automodule:: models
    :members:
    :undoc-members:
    :show-inheritance:

References
==========

.. [#neural_guide] `Neural Networks and Deep Learning <http://neuralnetworksanddeeplearning.com>`_