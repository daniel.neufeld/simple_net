Introduction
************

Code documentation is an often overlooked part of software development. It can seem a little time consuming at first, but properly documented code is an absolute must when working in a team environment. 

- It facilitates collaboration between developers
- It helps standardize coding practices with teammates
- It enhances the reusability of code
- It transferes knowledge to new teammmates

Sphinx is a Python package that aims to make code documentation as painless as possible. It leverages a simple document processing language called RestructuredText to translate plain text input files into well formatted HTML pages complete with indexing and search functionality. It also provides a tool called *autodoc* that scans your source code for properly formatted documentation (docstrings) at the module, class, and function level to generate API documentation with very little additional effort on your part. The next section outlines how to get up and running with Sphinx.