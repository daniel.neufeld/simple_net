Documenting Your Project
************************

The following sections show how to build your documentation by including, for example, explanations like this one, theory manuals, or anything else relevant to developers contributing to your project as well as the API documentation. Sphinx documentation uses a language called Restructured Text. Below are some basic examples on how to create formatted documents using the language. If you are familiar with Restrutured Text, you can skip to the :ref:`documentation_structure` section for details on how to start writing your docs.

Restructured Text
=================

Restructured Text is a system used for building formatted documents from plain text input files using special directives to indicate how the document should be rendered. Below are just a few examples of what you can do with Restructured Text to produce great looking documentation. Of course, you can always check out the source code of this or any of the other pages in this documentation by clicking the *View page source* button. The best way to learn Restructured Text is to simply copy source code from these or other examples. Check out reference [#rst_guide]_ for many more.

.. note::

    When indenting, use four spaces instead of tabs

Headers
-------

Chapter headings, section headings, and subsection headings have different underlines.

.. code-block::

    Chapter
    *******
    
    Section
    =======
    
    Subsection
    ----------
    
    Subsubsection
    ~~~~~~~~~~~~~
    
Tables
------

.. code-block:: rst

    +------------+------------+-----------+
    | Header 1   | Header 2   | Header 3  |
    +============+============+===========+
    | body row 1 | column 2   | column 3  |
    +------------+------------+-----------+
    | body row 2 | Cells may span columns.|
    +------------+------------+-----------+
    | body row 3 | Cells may  | - Cells   |
    +------------+ span rows. | - contain |
    | body row 4 |            | - blocks. |
    +------------+------------+-----------+


+------------+------------+-----------+
| Header 1   | Header 2   | Header 3  |
+============+============+===========+
| body row 1 | column 2   | column 3  |
+------------+------------+-----------+
| body row 2 | Cells may span columns.|
+------------+------------+-----------+
| body row 3 | Cells may  | - Cells   |
+------------+ span rows. | - contain |
| body row 4 |            | - blocks. |
+------------+------------+-----------+

Lists
-----

Enumerated lists:

.. code-block:: rst

    #. First point
    #. Second Point
    #. Third point

#. First point
#. Second Point
#. Third point

Bulleted lists:

.. code-block:: rst

    - First point
    - Second point
    - Third point

- First point
- Second point
- Third point

Images
------

Image without captions:

.. code-block:: rst

    .. image:: /images/barry.jpg
        :width: 300px
        :align: center

.. image:: /images/barry.jpg
    :width: 300px
    :align: center
    
.. code-block:: rst

    .. figure:: /images/jets.jpg
        :width: 500px
        :align: center
        
        The figure directive allows you to include captions and anything else along with your image.
    
.. figure:: /images/jets.jpg
    :width: 500px
    :align: center
    
    The figure directive allows you to include captions and anything else along with your image.

Equations
---------

Equations are entered using `Latex notation <https://en.wikibooks.org/wiki/LaTeX/Mathematics>`_ and the *math* directive. 

.. code-block:: rst

    .. math::

        \frac{\partial (\rho e)}{\partial t} + (\rho e+p)\frac{\partial u_{i}}{\partial x_{i}} = 
        \frac{\partial(\tau_{ij}u_{j})}{\partial x_{i}} + \rho f_{i}u_{i} +
        \frac{\partial(\dot{ q_{i}})}{\partial x_{i}} + r 

.. math::
    
    \frac{\partial (\rho e)}{\partial t} + (\rho e+p)\frac{\partial u_{i}}{\partial x_{i}} = \frac{\partial(\tau_{ij}u_{j})}{\partial x_{i}} + \rho f_{i}u_{i} + \frac{\partial(\dot{ q_{i}})}{\partial x_{i}} + r 

There are many `online tools <https://hostmath.com>`_ available that make writing Latex equations a little easier. Check out the Latex/Mathematics Wikibook for more on this topic [#latex_guide]_.

.. _documentation_structure:

Documentation Structure
=======================

After running *sphinx-quickstart*, the *source* folder inside your *docs* directory should contain *index.rst*. This is a Restructured Text file that serves as the hub for all of your project's documentation. You can write your docs directly in this file. However, it is often more convenient to refer seperate files containing docs for different sections of your project. The initial file contains the following.

.. code-block:: rst

    .. Simple Net documentation master file, created by
       sphinx-quickstart on Mon Sep 30 10:16:19 2019.
       You can adapt this file completely to your liking, but it should at least
       contain the root `toctree` directive.

    Simple Net Documentation
    ************************

    .. toctree::
        :maxdepth: 2
        :caption: Contents:

    Indices and tables
    ******************

    * :ref:`genindex`
    * :ref:`modindex`
    * :ref:`search`

The *toctree* directive creates a table of contents for your documentation. Under Indices and Tables, the three lines of code generate a search feature and index pages. Everyting you include in your docs should be placed under the *toctree* directive. Otherwise, although your docs may render, they won't be accessible from the main documentation *index.html*. Simple Net's documentation contains several chapters, each having their own *.rst* file. Sphinx will automatically scan and render every *.rst* file under *source*. Listing the file names **without** the exension will make sure they are linked and searchable in the main page of your documentation. The following is the contents of the *index.rst* file for this documentation.

.. code-block:: rst

    .. Simple Net documentation master file, created by
       sphinx-quickstart on Mon Sep 30 10:16:19 2019.
       You can adapt this file completely to your liking, but it should at least
       contain the root `toctree` directive.

    Simple Net Documentation
    ************************

    .. image:: /images/logo.png
        :width: 451px
        :alt: logo
        :align: center

    Simple Net is a python package intended to demonstrate API documentation using Sphinx.
    The package itself demonstrates how to develop a simple fully connected neural network
    from stcratch.

    .. toctree::
        :maxdepth: 2
        :caption: Contents:

        introduction
        sphinx
        documenting
        simple_net
        modules

    Indices and tables
    ******************

    * :ref:`genindex`
    * :ref:`modindex`
    * :ref:`search`


From the above code, you can see where the documentation title is shown along with an image and short description. Under the *toctree* directive, all the *.rst* files are listed in the order they are meant to be read. Try adding soemthing to your project's *index.rst* file and :ref:`generating_docs` to see what happens. 

.. _generating_docs:

Generating the Docs
===================

Commanding Sphinx to generate your documentation is done by entering the following command in your Anaconda terminal from within the *docs* folder.

.. code-block:: bash

    $ make html

You should see an output log in your terminal hopefully ending with *build succeeded*. You can now open the *build / html* folder. You should see several *.html* files assuming all went well with the build. You can access your documentation by opening *index.html* in any web browser. It is good practice to rebuild your docs frequently as you work on them to ensure that you can easily find and correct any mistakes in your *.rst* files that may cause errors during the build. The next section shows how to tell Sphinx to scan your code for docstrings to add API documentation to your docs. 

Autodoc
-------

Autodoc is a Sphinx extension that scans your modules for properly formatted docstrings to build API documentation. This is done by including the *automodule* directive in one of your *.rst* files. The Simple Net automodule commands are found in *simple_net.rst*.

.. code-block:: rst

    datasets
    ========

    .. automodule:: datasets
        :members:
        :undoc-members:
        :show-inheritance:

    functions
    =========

    .. automodule:: functions
        :members:
        :undoc-members:
        :show-inheritance:

    models
    ======

    .. automodule:: models
        :members:
        :undoc-members:
        :show-inheritance:

As you can see from the source code, you can enter the chapter title of the api as well as headings for each module along with explanations if you wish. The *automodule* directive itself has several options. The option :members: allows you to specify exactly which functions and classes inside your *.py* file you want documented by listing them as follows.  

.. code-block:: rst

    .. automodule:: functions
        :members: print_img, accuracy_score
        :undoc-members:
        :show-inheritance:

Leaving it blank will include them all (provided they have properly formatted docstrings). The *:undoc-members:* function can flag listed functions and clases to be excluded from the documentation. The *:show-inheritance:* option tells Sphinx to include the docs specified in the base class if your class inherits from one. Once you've turned on *automodule* for your modules, you can start adding :ref:`numpy_docs` to your code. If all goes well, they will be included in your documentation when you run *make html*.

.. _numpy_docs:

Numpy Docstrings
----------------

Docstrings are entered directly into your source code in comment blocks. All the features of Restructured Text are recognize, so you can include equations, images, code examples, tables, or any other feature that helps you communicate the usage of your functions and classes to your teammates. Below is an example of documenting a function belonging to Simple Net's *functions* module. 

.. code-block:: python

    def sigmoid(z):
        r"""
        Sigmoid activation function.

        .. math::

           \sigma (z) = \frac{1}{1 + e^{z}}
           
        Parameters
        ----------
        z: float
            Input value.

        Returns
        -------
        float
            Sigmoid at the input value.
        """
        return 1. / (1. + np.exp(-z))

.. note::

    The comment block containing the equation begins with *r"""*. This tells Python to interpret the string as literal. This is necessary because Latex equations use a lot of backslashes, and Sphinx normally will try to decode the the command (such as newline) instead of interpreting it as part of an equation.

To see how this renders, just enter *sigmoid* into the search bar on the left to see the resulting api docs for the function. Your docstrings should always include a description of the function at the top followed by a *Parameters* section. Here, each parameter of the function should be listed by *variable_name: type* followed by an indented paragraph that explains the meaning of the variable. If your function returns something, add a *Returns* section and list the types and descriptions of the returned variables. If you are using the Spyder IDE, you can highlight any function and hit *ctrl+alt+d* to automatically insert a docstring template for your function. Additionally, you can highlight your documented function or class and hit *ctrl+i*. This will render and display the function's documentation in the *help* window in the upper right. 

.. figure:: /images/docstring_screenshot.png
    :width: 1000px
    :align: center
    
    Hit *ctrl+i* on any function or object including your own to display the docs
    
You can add documention to your modules (any *.py* file included in your project) by entering comment blcoks at the top of the files. Have a look at any of the Simple Net modules for examples of this. Documenting classes is done in the same way as functions, entring a comment block just below the class definition. Have a look at the source code of :ref:`simple_net` to see examples of how this is typically done. 

References
==========

.. [#rst_guide] `Sphinx and RST syntax guide <http://thomas-cokelaer.info/tutorials/sphinx/index.html>`_
.. [#latex_guide] `Latex/Mathematics Wikibook <https://en.wikibooks.org/wiki/LaTeX/Mathematics>`_
