Using Sphinx
************

The following is a brief overview on getting Sphinx installed and configured. More details are avaialable in the official Sphinx documentation [#sphinx_guide]_. 

Setup Guide
===========

The following sections explain how to take your existing code and properly structure it as a Python project and set up the Sphinx documentation system. It will aslo demonstrate features of Restructured Text, the document processing language used by Sphinx to produce headers, lists, tables, images, math equations and so on. You can easily check the source code of each page in this documentation by clicking the "View page source" link at the top right. 

Project Structure
-----------------

The project should be structured along the following lines::

    project_folder
    ├── docs
    │   ├── build
    │   ├── source      
    │   │   ├── conf.py
    │   │   └── index.rst
    │   ├── make.bat
    │   └── Makefile
    ├── package_name
    │   ├── subfolder_1
    │   │   └── submodule_1.py
    │   ├── __init__.py
    │   ├── module1.py
    │   └── module2.py
    ├── .gitignore
    ├── install.md
    ├── README.md
    └── setup.py

Your *docs* folder can be left empty for now.  We'll use Sphinx's quickstart tool to generate the build and source folders and files. 

Installing Required Packages
----------------------------

Default Anaconda installations include Sphinx and Numpy by default. All you will need to build these documents on your local hardware is the "Read the Docs" theme, a widely used theme for Python packages. If you are using Miniconda, you may need to install Sphinx, Numpy, and the "Read the Docs" theme. You can install the packages by entering the following commands in your Anaconda terminal.

.. code-block:: bash

    $ conda install sphinx

.. code-block:: bash
    
    $ conda install sphinx_rtd_theme

.. code-block:: bash

    $ conda install numpy

Running Sphinx Quickstart
-------------------------

Sphinx includes a script to generate configuration files, the folder structure, and a basic index file for your documentation. In your Anaconda prompt, change to your empty docs folder and run the following command.

.. code-block:: bash

    $ sphinx-quickstart

The script will prompt you to define a few basic configuration options, the first of which is whether to separate build and source directories. The default is *no*, but it is important to change this to *yes* in order to separate the generated documents. This reason for this is that they need to be excluded from your project's git repository to avoid conflicts in the generated documentation files. The remaining options are straightforward and can be answered at your discretion.

Configuring the Docs
====================

You should see that the Sphinx Quickstart script has created a *build* and *source* folder inside your *docs* folder as well as *make.bat* and *Makefile*. We'll return to those files later. First, we need to take care of some additional configuration. Change into the *source* folder. You should find two files: *conf.py* and *index.rst*. The conf.py file contains all additional options we need to set. By default, the file looks like the following.

.. code-block:: python
    :linenos:
    
    # Configuration file for the Sphinx documentation builder.
    #
    # This file only contains a selection of the most common options. For a full
    # list see the documentation:
    # http://www.sphinx-doc.org/en/master/config

    # -- Path setup --------------------------------------------------------------

    # If extensions (or modules to document with autodoc) are in another directory,
    # add these directories to sys.path here. If the directory is relative to the
    # documentation root, use os.path.abspath to make it absolute, like shown here.
    #
    # import os
    # import sys
    # sys.path.insert(0, os.path.abspath('.'))


    # -- Project information -----------------------------------------------------

    project = 'My Project'
    copyright = '2019, My Name'
    author = 'My Name'

    # The full version, including alpha/beta/rc tags
    release = '1.0'


    # -- General configuration ---------------------------------------------------

    # Add any Sphinx extension module names here, as strings. They can be
    # extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
    # ones.
    extensions = [
    ]

    # Add any paths that contain templates here, relative to this directory.
    templates_path = ['_templates']

    # List of patterns, relative to source directory, that match files and
    # directories to ignore when looking for source files.
    # This pattern also affects html_static_path and html_extra_path.
    exclude_patterns = []


    # -- Options for HTML output -------------------------------------------------

    # The theme to use for HTML and HTML Help pages.  See the documentation for
    # a list of builtin themes.
    #
    html_theme = 'alabaster'

    # Add any paths that contain custom static files (such as style sheets) here,
    # relative to this directory. They are copied after the builtin static files,
    # so a file named "default.css" will overwrite the builtin "default.css".
    html_static_path = ['_static']

First, we need to make sure to add your project folders and all subfolders to the Python path so that Sphinx can find your modules and import their documentation. You can uncomment lines 13 to 15 to do this if your project has no subfolders. A more robust way to this is to use pathlib to detect the path of conf.py itself, and add all project folders and subfolders relative to this path. For this project, the lines of code from conf.py are as follows. 

.. code-block:: python
    
    from pathlib import Path
    import sys
    root_path = Path(__file__).absolute().parent.parent.parent
    sys.path.insert(0, str(root_path))
    sys.path.insert(0, str(root_path / 'simple_net'))
    
Be sure to add all subfolders of your project here including the root package folder to make sure all internal imports present in your modules work properly. Next, several extensions need to be enabled to both search your source code for Numpy style docstrings and parse them to build the API documentation. The *Autodoc* extension searches your code for docstrings and *Napoleon* parses Numpy style docstrings. Addtionally, *Mathjax* allows for Latex encoded math equations to be rendered. Extensions are enabled by adding to the *extensions* list defined on line 33 of *conf.py* as follows. 

.. code-block:: python

    extensions = ['sphinx.ext.autodoc', 'sphinx.ext.napoleon', 'sphinx.ext.mathjax']

The theme of the HTML documentation is set on line 50. A theme called *Read The Docs* is fairly standard in technical computing oriented Python packages. It is designed to render well on both desktop and mobile devices. The theme can be enabled by changing line 50 to the following.

.. code-block:: python
    
    html_theme = 'sphinx_rtd_theme'

Finally, any additional options can be specified by setting their variables in the section under line 117. A logo for the project can be set which dispalys an image file on the top left underneath the project name. A logo can also be displayed on the browser tab. Adding the neural net image for this project was done using the following lines of code entered below the html_theme variable. 

.. code-block:: python

    html_logo = str(root_path / 'docs' / 'source' / 'images' / 'icon.png')
    html_favicon = str(root_path / 'docs' / 'source' / 'images' / 'icon.png')

`Many additional options <https://www.sphinx-doc.org/en/master/usage/configuration.html>`_ not covered here are available to further customize the documentation. 

References
==========

.. [#sphinx_guide] `Sphinx Quickstart Guide <http://www.sphinx-doc.org/en/master/usage/quickstart.html>`_
