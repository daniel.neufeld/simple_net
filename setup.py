# -*- coding: utf-8 -*-
"""
Created on Wed Aug 28 09:27:09 2019

@author: daniel.neufeld
"""


from pathlib import Path
import sys
import site


st_pkg = Path(site.getsitepackages()[-1])
root = Path(__file__).absolute().parent
paths = []
paths.append(root)

print(st_pkg)
print(paths)
with open(str(st_pkg / 'sphinx_tutorial.pth'), 'w') as FILE:
    for pth in paths:
        FILE.write(str(pth))
