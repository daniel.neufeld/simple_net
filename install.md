# Install Environment

 The package dependancies can be installed by entering the following commands in your Anaconda environment command prompt.
 
 `$ conda install numpy`

 `$ conda install sphinx`

 `$ conda install sphinx_rtd_theme`

### Register Simple Net in your Python Environment

 In your Anaconda command prompt, change to the Simple Net folder and run the following:

 `$ python setup.py`

### Build documentation files

 You can build documentation files by running `make html` in the repository docs/ 

 The docs will then be available in *docs/build/html/index.html*
