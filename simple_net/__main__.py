# -*- coding: utf-8 -*-
"""
Created on Wed Sep 25 14:32:44 2019

@author: daniel.neufeld

This app uses the simple_net modules to build and train a neural net for image
recognition to demonstrate the usage of this packge.
"""
from pathlib import Path
from simple_net.datasets import DataSet, print_img
from simple_net.models import LinearNet
from simple_net import functions
import numpy as np


print('Sphinx demo app')
print('This demo will load a dataset of 1797 8x8 handwritten number images')
print('and train a fully connected neural net to detect the numbers drawn in')
print('the images.')
PATH = Path(__file__).absolute().parent / 'data' / 'digits_data.pkl'
np.seterr(all='ignore')
DATA = DataSet(PATH)
TRAIN, VALID = DATA.get_data(0.8)
IMAGES, TARGETS = TRAIN
LAYER_SIZES = [len(IMAGES[0]), 30, len(TARGETS[0])]
NN = LinearNet(LAYER_SIZES, functions.sigmoid, functions.sigmoid_prime,
               functions.mse_loss_prime)
NN.train(IMAGES, TARGETS)
IMAGES, TARGETS = VALID
print('Training Complete! Here are a few sample predictions.')
for INDEX in range(len(IMAGES)):
    print('-----------------------------------------------------------')
    OUT = NN.forward(IMAGES[INDEX])
    PRED = DATA.decode(OUT)
    ACT = DATA.decode(TARGETS[INDEX])
    RES = PRED == ACT
    print(f'Prediction = {PRED}, Target = {ACT}, Result = {RES}')
    print_img(IMAGES[INDEX].reshape(DATA.data_shape))
    INP = input('Hit <ENTER> for another prediction, q to quit > ')
    if INP == 'q':
        break
