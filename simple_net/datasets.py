# -*- coding: utf-8 -*-
"""
Created on Wed Sep 25 14:32:44 2019

@author: daniel.neufeld

The datasets module contains classs for loading and preparing image data for
training and predicting using neural networks.
"""
import pickle
import gzip
from random import shuffle
import numpy as np


class DataSet():
    """
    Load and processes data sets for training neural networks.

    The file format is a gzipped binary pickle dump containing the data.
    The data is stored in a tuple containing the list of data entries and a
    list of the corresponding ground truth values.

    Parameters
    ----------
    data_path: path-like
        Path to the database pickle file.
    """

    def __init__(self, data_path):
        self.data_path = data_path
        self._load_data()
        self._normalize_data()
        self._encode_targets()
        self.data_shape = self[0][0].shape

    def _load_data(self):
        try:
            with gzip.open(self.data_path, 'rb') as file:
                data_targets = pickle.load(file)
        except Exception as ex:
            print('Data file not found!')
            raise ex
        self.data, self.targets = data_targets

    def _normalize_data(self):
        ubnd = self.data.max()
        lbnd = self.data.min()
        self.data = (self.data - lbnd) / (ubnd - lbnd)

    def _encode_targets(self):
        classes = np.sort(np.unique(self.targets))
        targets_enc = np.zeros((len(self.targets), len(classes)))
        for tgt, t_enc in zip(self.targets, targets_enc):
            idx = np.argwhere(classes == tgt).flatten()[0]
            t_enc[idx] = 1
        self.classes = classes
        self.targets = targets_enc

    def decode(self, x_vec):
        """
        Decode an input vector and returns the value.

        Parameters
        ----------
        x_vec: array
            Vector to be decoded.

        Returns
        -------
        class
            Decoded class value.
        """
        return self.classes[np.argmax(x_vec)]

    def get_data(self, split=0.8, flatten=True):
        """
        Return a randomly sampled set of training and test data.

        Parameters
        ----------
        split: float, optional
            Ratio of training data to . The default is 0.8.
        flatten: TYPE, optional
            Set to true to flatten the data. The default is True.

        Returns
        -------
        train: TYPE
            Training data set.
        valid: TYPE
            Validation data set.
        """
        try:
            assert 0 < split < 1
        except AssertionError as err:
            print('Split must be > 0 and < 1')
            raise err
        dat = self.data
        if flatten:
            dat = dat.reshape(dat.shape[0], np.prod(dat.shape[1:]))
        i = int(len(self.data) * split)
        idx = np.array(range(len(self.data)))
        shuffle(idx)
        train = (dat[idx[0:i]], self.targets[idx[0:i]])
        valid = (dat[idx[i:]], self.targets[idx[i:]])
        return train, valid

    def __getitem__(self, idx):
        """
        Return one data entry and its corresponding ground truth value.

        Parameters
        ----------
        idx: int
            Index of the data element.

        Returns
        -------
        array
            Array containing the data element.
        array
            Array containing the encoded ground truth value.
        """
        return self.data[idx], self.targets[idx]

    def __len__(self):
        """Return the number of data entries."""
        return len(self.data)


def print_img(image_matrix):
    """
    Display a normalized greyscale image using characters.

    Parameters
    ----------
    image_matrix: array
        Matrix of greyscale values.
    """
    chars = ' ░▒▓█'
    out = ['┌' + '─' * image_matrix.shape[0] * 2 + '┐']
    line = '│'
    test = [i / len(chars) for i in range(len(chars) + 1)]
    for val in image_matrix.flatten():
        for i in range(1, len(test)):
            if test[i - 1] <= val <= test[i]:
                line += chars[i - 1] * 2
        if len(line) == 2 * image_matrix.shape[0] + 1:
            out.append(line + '│')
            line = '│'
    out.append('└' + '─' * image_matrix.shape[0] * 2 + '┘')
    [print(o) for o in out]


if __name__ == '__main__':
    from pathlib import Path
    PATH = Path(__file__).absolute().parent / 'data' / 'digits_data.pkl'
    DS = DataSet(PATH)
    IDX = np.random.randint(0, len(DS))
    IM, TGT = DS[IDX]
    print(TGT)
    print(DS.decode(TGT))
    print_img(IM)
