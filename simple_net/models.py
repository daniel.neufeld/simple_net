# -*- coding: utf-8 -*-
"""
Created on Fri Aug 23 09:14:44 2019

@author: Bethany Lochbihler, Daniel Neufeld

This module contains classes and functions for building a fully-conneted neural
network.
"""
import math
import random
import numpy as np
from simple_net import functions


class LinearNet():
    """
    Creates a linear neural network.

    Parameters
    ----------
    layer_sizes: array_like
        Array containing sizes of input, hidden, and output layers.
    activation_fn: function
        Activation function.
    activation_fn_prime: function
        Function returning the derivative of the activation function.
    loss_fn: function
        Loss function.
    loss_fn_prime: TYPE
        Function returning the derivative of the loss function.
    eta: float, optional
        Learning rate. The default is 1..
    """

    def __init__(self, layer_sizes, activation_fn, activation_fn_prime,
                 loss_fn_prime, eta=1.):
        self.Y_hat_bin = None
        self.a = None
        self.num_layers = len(layer_sizes)
        self.biases = [np.random.randn(n, 1) for n in layer_sizes[1:]]
        self.weights = [np.random.randn(n, z)
                        for n, z in zip(layer_sizes[1:], layer_sizes[:-1])]
        self.act_fn = activation_fn
        self.act_fcn_prime = activation_fn_prime
        self.loss_fn_prime = loss_fn_prime
        self.delta_b0 = [np.zeros((n, 1)) for n in layer_sizes[1:]]
        self.delta_w0 = [np.zeros((n, z))
                         for n, z in zip(layer_sizes[1:], layer_sizes[:-1])]
        self.eta = eta

    def train(self, X, Y, epochs=30, batch_size=10):
        """
        Trains the neural net.

        Parameters
        ----------
        X: array
            Array of input training data.
        Y: array
            Array of encoded true values.
        epochs: int, optional
            Number of training epochs. The default is 30.
        batch_size: int, optional
            Batch size. The default is 10.
        """
        loss = {}
        print(f'{"Epoch":>10}{"Loss":>10}{"Accuracy":>10}')
        for e in range(epochs):
            train_data = list(zip(X, Y))
            random.shuffle(train_data)
            for k in range(math.ceil(len(train_data) / batch_size)):
                batch_data = train_data[k * batch_size:(k + 1) * batch_size]
                self.update_batch(batch_data)
            Y_hat = self.predict(X)
            loss[e] = functions.mean_squared_error(Y_hat, Y)
            acc = self.accuracy(Y, Y_hat)
            if e % 1 == 0:
                print(f'{e:>10d}{loss[e]:>10.4f}{acc:>10.4f}')

    def predict(self, X):
        """
        Return a prediction based on input data.

        Parameters
        ----------
        X : array
            Input data.

        Returns
        -------
        array
            Array of predicted values.
        """
        Y_hat = []
        for x in X:
            Y_hat.append(self.forward(x))
        return np.array(Y_hat).squeeze()

    def accuracy(self, Y, Y_hat):
        """
        Retruns the accuracy of a list of predictions vs. a list of true vals.

        Parameters
        ----------
        Y : array
            Array of predictions.
        Y_hat : array
            Array of encoded true values.

        Returns
        -------
        acc : float
            Accuracy of the predictions.
        """
        if Y.ndim == 1:
            Y_hat_bin = (Y_hat >= 0.5).astype('int').ravel()
        else:
            Y_hat_bin = np.squeeze(np.zeros_like(Y_hat))
            Y_hat_bin[np.arange(len(Y_hat)), Y_hat.argmax(1)] = 1
        acc = functions.accuracy_score(Y_hat_bin, Y)
        self.Y_hat_bin = Y_hat_bin
        return acc

    def update_batch(self, train_data):
        """
        Trains and updates the neural net for a given batch of training data.

        Parameters
        ----------
        train_data : array
            Training data.
        """
        delta_B = self.delta_b0.copy()
        delta_W = self.delta_w0.copy()

        for x, y in train_data:
            self.forward(x)
            delta_b, delta_w = self.gradient(y)
            delta_B = [delta_B[i] + delta_b[i]
                       for i in range(self.num_layers - 1)]
            delta_W = [delta_W[i] + delta_w[i]
                       for i in range(self.num_layers - 1)]

        for j in range(self.num_layers - 1):
            self.biases[j] -= self.eta / len(train_data) * delta_B[j]
            self.weights[j] -= self.eta / len(train_data) * delta_W[j]

    def gradient(self, y):
        """
        Compute the gradients of weights and biases.

        Parameters
        ----------
        y : array
            Array of batch true values.

        Returns
        -------
        delta_b : array
            Gradient of the biases.
        delta_w : array
            Gradient of the weights.
        """
        delta_b = self.delta_b0.copy()
        delta_w = self.delta_w0.copy()
        delta = self.loss_fn_prime(self.a[-1], y.reshape(self.a[-1].shape))

        for i in range(self.num_layers - 2, -1, -1):
            delta_b[i] = delta
            delta_w[i] = np.dot(delta, self.a[i].T)
            delta = np.dot(self.weights[i].T, delta) * self.act_fcn_prime(self.a[i])
        return delta_b, delta_w

    def forward(self, X):
        """
        Feed input layer data forward through the netowrk.

        Parameters
        ----------
        X : array
            Input layer data.

        Returns
        -------
        array
            Output layer values.
        """
        self.a = [X.reshape(-1, 1)]
        for b, w in zip(self.biases, self.weights):
            self.a.append(self.act_fn(functions.zed(w, b, self.a[-1])))
        return self.a[-1]

    def __call__(self, X):
        """
        Predict at the input vector.

        Parameters
        ----------
        X : array
            Array representing the input data.

        Returns
        -------
        array
            Encoded prediction.

        """
        return self.forward(X)


if __name__ == '__main__':
    from pathlib import Path
    from simple_net import datasets
    PATH = Path(__file__).absolute().parent / 'data' / 'digits_data.pkl'
    np.seterr(all='ignore')
    DATA = datasets.DataSet(PATH)
    TRAIN, VALID = DATA.get_data(0.8)
    IMAGES, TARGETS = TRAIN
    LAYER_SIZES = [len(IMAGES[0]), 30, len(TARGETS[0])]
    NN = LinearNet(LAYER_SIZES, functions.sigmoid, functions.sigmoid_prime,
                   functions.mse_loss_prime)
    NN.train(IMAGES, TARGETS)
    IMAGES, TARGETS = VALID
    for I, IMAGE in enumerate(IMAGES):
        datasets.print_img(IMAGE.reshape(DATA.data_shape))
        OUT = NN.forward(IMAGE)
        print(DATA.decode(OUT))
        print(DATA.decode(TARGETS[I]))
