# -*- coding: utf-8 -*-
"""
Created on Fri Sep 27 10:31:01 2019

@author: daniel.neufeld

This module contains activation and loss functions and their derivatives for
defining fully-connected neural networks.
"""

import numpy as np


def accuracy_score(y_true, y_pred):
    """
    Calculate the accuracy of a list of predictions vs. ground truth.

    Parameters
    ----------
    y_true: array
        List of true values.
    y_pred: array
        List of predicted values.

    Returns
    -------
    float
        The accuracy of the predictions.
    """
    count = 0
    for ytrue, ypred in zip(y_true, y_pred):
        if np.all(ytrue == ypred):
            count += 1
    return count / len(y_true)


def mean_squared_error(y_vec1, y_vec2):
    r"""
    Calculate the mean squared error between two vectors of equal length.

    .. math::

        MSE = \frac {1}{n} \sum_{i=1}^{n} (Y_i - \hat{Y}_i) ^ 2

    Parameters
    ----------
    y_vec1: array
        Array 1.
    y_vec2: array
        Array 2.

    Returns
    -------
    float
        Mean squared error.
    """
    return np.sum((y_vec1 - y_vec2) ** 2) / len(y_vec1)


def sigmoid(z):
    r"""
    Sigmoid activation function.

    .. math::

       \sigma (z) = \frac{1}{1 + e^{z}}

    Parameters
    ----------
    z: float
        Input value.

    Returns
    -------
    float
        Sigmoid at the input value.
    """
    return 1. / (1. + np.exp(-z))


def sigmoid_prime(z):
    r"""
    Sigmoid activation function derivative.

    .. math::

       \sigma' (z) = \sigma (z) \left ( 1 - \sigma (z) \right )

    Parameters
    ----------
    z: float
        Input value.

    Returns
    -------
    float
        Derivative of the sigmoid function at the input value.
    """
    return sigmoid(z) * (1. - sigmoid(z))


def zed(w_vec, b_vec, x_vec):
    r"""
    Calculate the total input to a layer at an input vector.

    .. math::

        \bar{z} = \bar{w} \cdot \bar{x} + \bar{b}

    Parameters
    ----------
    w_vec: array
        Array of weight values.
    b_vec: array
        Array of neuron bias.
    x_vec: array
        Input vector.

    Returns
    -------
    array
        Neuron state values of the given layer.
    """
    return np.dot(w_vec, x_vec) + b_vec


def mse_loss_prime(y_hat, y_vec):
    """
    Calculate the mean squared error loss.

    Parameters
    ----------
    y_vec: array
        Array of encoded true values.
    act_fcn: function
        Activation function.
    act_fcn_prime: fuinction
        Activation function derivative.

    Returns
    -------
    float
        Loss funtion gradient.
    """
    return y_hat - y_vec
